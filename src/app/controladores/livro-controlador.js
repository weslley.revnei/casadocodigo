const { validationResult } = require('express-validator/check');

const LivroDao = require('../infra/livro-dao');
const db = require('../../config/database');

class LivroControlador {

    static rotas() {
        return {
            lista: '/livros',
            cadastro: '/form',
            edicao: '/form/:id',
            delecao: '/livros/:id'
        }
    }

    home() {

        return function (req, resp) {
            resp.marko(
                require('../views/base/home/home.marko')
            );
        }

    }

    lista() {
        return function (req, resp) {

            const livroDao = new LivroDao(db);
            livroDao.lista()
                .then(livros => resp.marko(
                    require('../views/livros/lista/lista.marko'),
                    {
                        livros: livros
                    }
                ))
                .catch(erro => console.log(erro));
        };
    }

    cadastrar() {

        return function (req, resp) {

            const livroDao = new LivroDao(db);

            const erros = validationResult(req);

            if (!erros.isEmpty()) {
                return resp.marko(
                    require('../views/livros/form/form.marko'),
                    {
                        livro: {},
                        errosValidacao: erros.array()
                    }
                );
            }

            livroDao.adiciona(req.body)
                .then(resp.redirect(LivroControlador.rotas().lista))
                .catch(erro => console.log(erro));
        }

    }

    editar() {

        return function (req, resp) {
            console.log(req.body);
            const livroDao = new LivroDao(db);

            const erros = validationResult(req);

            if (!erros.isEmpty()) {
                return resp.marko(
                    require('../views/livros/form/form.marko'),
                    {
                        livro: {},
                        errosValidacao: erros.array()
                    }
                );
            }

            livroDao.atualiza(req.body)
                .then(resp.redirect(LivroControlador.rotas().lista))
                .catch(erro => console.log(erro));
        }

    }

    formularioEdicao() {

        return function (req, resp) {
            resp.marko(require('../views/livros/form/form.marko'), { livro: {} });
        }

    }

    buscaPorId() {

        return function (req, resp) {
            const id = req.params.id;
            const livroDao = new LivroDao(db);

            livroDao.buscaPorId(id)
                .then(livro =>
                    resp.marko(
                        require('../views/livros/form/form.marko'),
                        { livro: livro }
                    )
                )
                .catch(erro => console.log(erro));
        }

    }

    deletaLivro() {

        return function (req, resp) {
            const id = req.params.id;

            const livroDao = new LivroDao(db);
            livroDao.remove(id)
                .then(() => resp.status(200).end())
                .catch(erro => console.log(erro));
        }

    }

}

module.exports = LivroControlador;