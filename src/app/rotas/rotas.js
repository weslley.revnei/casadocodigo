const { check, validationResult } = require('express-validator/check');

const LivroDao = require('../infra/livro-dao');
const db = require('../../config/database');

const LivroControlador = require('../controladores/livro-controlador');
const livroControlador = new LivroControlador();

module.exports = (app) => {

    const rotasLivros = LivroControlador.rotas();

    app.get('/', livroControlador.lista());

    app.get(rotasLivros.lista, livroControlador.lista());

    app.post(rotasLivros.lista, [
        check('titulo').isLength({ min: 5 }).withMessage('O título precisa ter no mínimo 5 caracteres!'),
        check('preco').isCurrency().withMessage('O preço precisa ter um valor monetário válido!')
    ],
        livroControlador.cadastrar()
    );

    app.put(rotasLivros.lista, [
        check('titulo').isLength({ min: 5 }).withMessage('O título precisa ter no mínimo 5 caracteres!'),
        check('preco').isCurrency().withMessage('O preço precisa ter um valor monetário válido!')
    ],
        livroControlador.editar()
    );

    app.get(rotasLivros.cadastro, livroControlador.formularioEdicao());

    app.get(rotasLivros.edicao, livroControlador.buscaPorId());

    app.delete(rotasLivros.delecao, livroControlador.deletaLivro());
};